define(['../helpers/all.js'], function (helpers) {
    'use strict';

    var fixtureElement;

    beforeEach(function () {
        fixtureElement = document.getElementById('test-fixture');
        if (!fixtureElement) {
            fixtureElement = document.createElement('div');
            fixtureElement.id = 'test-fixture';
            document.body.appendChild(fixtureElement);
        }
        fixtureElement.innerHTML = '';
    });

    afterEach(function () {
        fixtureElement.innerHTML = '';
        helpers.warnIfLayersExist();
    });
});
