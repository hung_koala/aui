(function () {

    var css = AJS.deprecate.css;
    var fiveNineZero = {
        // Inline Dialog
        'arrow': 'aui-inline-dialog-arrow',
        'contents': 'aui-inline-dialog-contents',

        // Messages
        'error': 'aui-message-error',
        'generic': 'aui-message-generic',
        'hint': 'aui-message-hint',
        'info': 'aui-message-info',
        'success': 'aui-message-success',
        'warning': 'aui-message-warning'
    };
    var name;

    for (name in fiveNineZero) {
        if (Object.hasOwnProperty.call(fiveNineZero, name)) {
            css(name, {
                alternativeName: fiveNineZero[name],
                removeVersion: '6.0.0',
                sinceVersion: '5.9.0'
            });
        }
    }

})();
