;(function(init) {
    'use strict';

    var ret = init(AJS.$, window.skate);
    define(function() {
        return ret;
    });
})(function ($, skate) {
    'use strict';


    function findControlled(trigger) {
        return document.getElementById(trigger.getAttribute('aria-controls'));
    }

    function triggerMessage(trigger, e) {
        if (trigger.isEnabled()) {
            var component = findControlled(trigger);
            if (component && component.message) {
                component.message(e);
            }
        }
    }

    skate('data-aui-trigger', {
        type: skate.types.ATTR,
        events: {
            click: function(trigger, e) {
                triggerMessage(trigger, e);
                e.preventDefault();
            },
            mouseenter: function(trigger, e) {
                triggerMessage(trigger, e);
            },
            mouseleave: function(trigger, e) {
                triggerMessage(trigger, e);
            },
            focus: function(trigger, e) {
                triggerMessage(trigger, e);
            },
            blur: function(trigger, e) {
                triggerMessage(trigger, e);
            }
        },
        prototype: {
            disable: function() {
                this.setAttribute('aria-disabled', 'true');
            },
            enable: function() {
                this.setAttribute('aria-disabled', 'false');
            },
            isEnabled: function() {
                return this.getAttribute('aria-disabled') !== 'true';
            }
        }
    });
});
