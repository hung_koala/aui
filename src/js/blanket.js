(function($) {
    
    var $overflowEl;

    /**
     *
     * Dims the screen using a blanket div
     * @param useShim deprecated, it is calculated by dim() now
     */
    AJS.dim = function (useShim, zIndex) {

        if  (!$overflowEl) {
            $overflowEl = $(document.body);
        }

        if (useShim === true) {
            useShimDeprecationLogger();
        }

        var isBlanketShowing = (!!AJS.dim.$dim) && AJS.dim.$dim.attr('aria-hidden') === 'false';

        if(!!AJS.dim.$dim) {
            AJS.dim.$dim.remove();
            AJS.dim.$dim = null;
        }

        AJS.dim.$dim = AJS("div").addClass("aui-blanket");
        AJS.dim.$dim.attr('tabindex', '0'); //required, or the last element's focusout event will go to the browser
        AJS.dim.$dim.appendTo(document.body);

        if (!isBlanketShowing) {
            //recompute after insertion and before setting aria-hidden=false to ensure we calculate a difference in
            //computed styles
            AJS._internal.animation.recomputeStyle(AJS.dim.$dim);

            AJS.dim.cachedOverflow = $overflowEl.css("overflow");
            $overflowEl.css("overflow", "hidden");
        }

        AJS.dim.$dim.attr('aria-hidden', 'false');

        if (zIndex) {
            AJS.dim.$dim.css({zIndex: zIndex});
        }

        return AJS.dim.$dim;
    };

    /**
     * Removes semitransparent DIV
     * @see AJS.dim
     */
    AJS.undim = function() {
        if (AJS.dim.$dim) {
            AJS.dim.$dim.attr('aria-hidden', 'true');

            $overflowEl && $overflowEl.css("overflow",  AJS.dim.cachedOverflow);
        }
    };

    var useShimDeprecationLogger = AJS.deprecate.getMessageLogger('useShim', {
        extraInfo: 'useShim has no alternative as it is now calculated by dim().'
    });

}(AJS.$));